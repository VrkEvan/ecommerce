<?php

namespace VCEP\CommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VCEPCommerceBundle:Default:index.html.twig');
    }
}
