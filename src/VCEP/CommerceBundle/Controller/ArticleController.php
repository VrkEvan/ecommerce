<?php

namespace VCEP\CommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    public function indexAction($id)
    {
    	//Entity manager, permet de rechercher les entités
    	$em = $this->getDoctrine()->getManager();

    	// On recherche l'article d'id 'id'
    	$article = $em->getRepository('VCEPCommerceBundle:Article')->find($id);

    	//On envoie l'article d'id 'id' vers la vue index.html.twig du dossier VCEP/CommerceBundle/Resources/views/Article 
    	// et on attribue à la variable article de la vue, l'objet $article
        return $this->render('VCEPCommerceBundle:Article:index.html.twig', array('article' => $article));
    }
}
