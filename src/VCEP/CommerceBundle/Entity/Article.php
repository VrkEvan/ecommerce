<?php


namespace VCEP\CommerceBundle\Entity;


// On définit le namespace des annotations utilisées par Doctrine2

// En effet, il existe d'autres annotations, on le verra par la suite,

// qui utiliseront un autre namespace

use Doctrine\ORM\Mapping as ORM;


/**

 * @ORM\Entity

 */

class Article

{

  /**

   * @ORM\Column(name="id", type="integer")

   * @ORM\Id

   * @ORM\GeneratedValue(strategy="AUTO")

   */

  protected $id;


  /**

   * @ORM\Column(name="date", type="date")

   */

  protected $date;


  /**

   * @ORM\Column(name="title", type="string", length=255)

   */

  protected $title;


  /**

   * @ORM\Column(name="author", type="string", length=255)

   */

  protected $author;


  /**

   * @ORM\Column(name="content", type="text")

   */

  protected $content;


  // Les getters

  // Les setters


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
}
